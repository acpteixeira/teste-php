<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { // função para criar a tabela de LIVROS
        Schema::create('livros', function (Blueprint $table) {
            $table->increments('id'); // código do livro
            $table->string('titulo',50); // título do livro
            $table->integer('ano',4); // ano do livro

            $table->unsignedInteger('locacao_id');
            $table->foreign('locacao_id','data_retirada', 'data_devolucao')->references('id')->on('locacaos')->onDelete('cascade');
        
            $table->timestamps(); // cria create_at e update_at no banco
            $table->SoftDeletes(); // cria delete_at, mostra que foi deletado sem apagar o registro
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { // essa função dropa a tabela criada anteriormente
        Schema::dropIfExists('livros');
    }
}
