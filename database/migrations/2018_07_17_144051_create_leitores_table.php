<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeitoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { // função para criar a tabela de LEITORES
        Schema::create('leitores', function (Blueprint $table) {
            $table->increments('id'); // código do leitor
            $table->string('nome',50); // nome do leitor
            $table->string('email')->unique();

            $table->unsignedInteger('locacao_id');
            $table->foreign('locacao_id','data_retirada', 'data_devolucao')->references('id')->on('locacaos')->onDelete('cascade');

            $table->timestamps(); // cria create_at e update_at no banco
            $table->SoftDeletes(); // cria delete_at, mostra que foi deletado sem apagar o registro

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { // essa função dropa a tabela criada anteriormente
        Schema::dropIfExists('leitores');
    }
}
