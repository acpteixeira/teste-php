<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { // função para criar a tabela de LOCACOES
        Schema::create('locacaos', function (Blueprint $table) {
            $table->increments('id'); // código da locacao
            $table->date('retirada')->nullable(); // data de retirada, pode ser null
            $table->date('devolucao')->nullable(); // data de devolução, pode ser null


            $table->unsignedInteger('leitor_id');
            $table->foreign('leitor_id','nome')->references('id')->on('leitores')->onDelete('cascade');

            $table->timestamps(); // cria create_at e update_at no banco
            $table->SoftDeletes(); // cria delete_at, mostra que foi deletado sem apagar o registro
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { // essa função dropa a tabela criada anteriormente
        Schema::dropIfExists('locacaos');
    }
}
