_____________________________________________________________________________
| __        __                                                    __  __      |
||__) |__| |__)    _|_   |    _   _  _      _ |    _|_   |\/|    (_  /  \ |   |
||    |  | |        |    |__ (_| |  (_| \/ (- |     |    |  | \/ __) \_\/ |__ |
|_____________________________________________________________________________|

1. Introdu��o
	O PHP (Hypertext Preprocessor) � uma linguagem de script open source de uso geral, muito utilizada e especialmente adequada para o desenvolvimento web e que pode ser embutida dentro do HTML. A principal diferen�a entre o PHP e o JavaScript, por exemplo, � que no lado do cliente o c�digo � executado no servidor, gerando o HTML que � ent�o enviado para o navegador. O navegador recebe os resultados da execu��o desse script, mas n�o sabe qual era o c�digo fonte (fonte: Manual do PHP - php.net/manual).
	O Laravel � um Framework PHP utilizado para o desenvolvimento web, que utiliza a arquitetura MVC e tem como principal caracter�stica ajudar a desenvolver aplica��es seguras e perform�ticas de forma r�pida, com c�digo limpo e simples (fonte: Devmedia - Laravel Tutorial). A documenta��o � intuitiva e de f�cil entendimento, facilitando a utiliza��o da ferramenta.
	E por �ltimo, mas n�o menos importante, o MySQL � um sistema de gerenciamento de banco de dados (SGBD), que utiliza a linguagem SQL (Linguagem de Consulta Estruturada, do ingl�s Structured Query Language) como interface. � atualmente um dos sistemas de gerenciamento de bancos de dados mais populares, com mais de 10 milh�es de instala��es pelo mundo.
	Este projeto � um sistema b�sico de gerenciamento de livros da Biblioteca XYZ.

2. Requisitos
	A. Ser desenvolvido em PHP com Laravel;
	B. Utilizar banco de dados MySQL.

3. Telas:
	A. Cadastro/Edi��o/Remo��o de Leitor (nome + e-mail) [URL: /leitores];
	B. Cadastro/Edi��o/Remo��o de Livro (t�tulo + ano) [URL: /livros];
	C. Hist�rico de Loca��es de um livro (quem locou + data da retirada + data da devolu��o) [URL: /livros/c�digo OU ID do livro];

4. Regras:
	A. Nova loca��o: o leitor pode locar um livro;
	B. Cada livro s� possui 1 exemplar;
	C. Cada livro s� pode ser locado por um leitor por vez;
	D. Cada loca��o deve ter uma data de retirada;
	E. Cada loca��o deve ter uma data de devolu��o.

5. Desenvolvimento:
	* Vers�es utilizadas: Laravel (5.6.27), PHP (7.2.7), Composer (1.6.5), XAMPP (3.2.2), Google Chrome (67.0.3396.99 64bit) e Sublime Text (3.1.1 Build 3176).
	
	//	cmd [modo admin] na pasta C:\xampp\htdocs
	// 	cmd > laravel new bibliotecaXYZ
	//	startar apache e MySQL no XAMPP
	//	no browser, abrir localhost para ver o dashboard do XAMPP
	//	ainda no browser, acessar localhost/bibliotecaXYZ/public/ e ver logo do Laravel
	//	na pasta do projeto, atualizar o composer: cmd > composer update --no-scripts
	//		
	//	cdm > php artisan make: auth (utilizado em novas aplica��es, instalar� uma visuliza��o de layout, registro e login, al�m de rotdas para endpoins de 		autentica��o. Tamb�m ser� gerado um HomeController para manipular solicita��es p�s-login.
	//	
	//	gerar chave de aplicativo [cmd > php artisan: generate]
	//	a chave � definida no arquivo .env e j� foi configurada neste caso, pois o Laravel foi instalado pelo cmd
	//	
	//	configurar BD
	//	arquivo .env, ver:
	//	DB_CONNECTION=mysql
	//	DB_HOST=127.0.0.1
	//	DB_PORT=3306
	//	DB_DATABASE=bibliotecaXYZ
	//	DB_USERNAME=root
	//	DB_PASSWORD=
	//	
	//	criar o BD na p�gina phpmyadmin
	//	
	//	cmd > php artisan migrate
	//	
	//	cmd > php artisan make:migration create_livros_table
	//	cmd > php artisan make:model Livro
	//	editar arquivos de livros e Livro
	//	
	//	cmd > php artisan make:model Locacao -m
	//	foi criado o modelo Locacao e a tabela LOCACAOS (automaticamente por conta do -m)
	//	editar respectivos arquivos
	//	
	//	cmd > php artisan make:controller LocacaoController
	//	cmd > php artisan make:controller LivroController
	//	cmd > php artisan make:controller LeitorController
	//	editar respectivos arquivos
	//	
	//	adicionado o arquivo C:\xampp\htdocs\bibliotecaXYZ\app\Http\Routes.php
	//

	//	Em desenvolvimento...

6. Execu��o e Testes:
	Em desenvolvimento...