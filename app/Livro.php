<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

// cada tabela de banco de dados tem um Model correspondente que é usado para interagir com essa tabela

class Livro extends Model
{	
    use Notifiable;

    protected $fillable = ['titulo','ano']; // campos que podem ser inseridos pelo usuário do sistema
    protected $table = 'livros';
}
