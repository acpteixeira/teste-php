<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

// cada tabela de banco de dados tem um Model correspondente que é usado para interagir com essa tabela

class Locacao extends Model
{
    use Notifiable;

    protected $fillable = ['retirada','devolucao','leitor_id']; // campos que podem ser inseridos pelo usuário do sistema
    protected $table = 'locacaos';

    // ver o usuário associado a locacao
    public function historico_locacao()
    {
        return $this->hasOne('App\Leitor');
    }

}
