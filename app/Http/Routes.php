<?php

use App\Leitor;
use App\Livro;

use Illuminate\Http\Request;

/**
 * Display All Tasks
*/ 
Route::get('/', function () {
    //
    // pode ser algo do tipo return view('X');
    $leitores = Leitor::orderBy('created_at', 'asc')->get();

    return view('leitores', [
        'leitores' => $leitores
    ]);
});


/**
 * Add A New Task
 */
Route::post('/leitores', function (Request $request) {
    //
    $validator = Validator::make($request->all(), [
        'nome' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    // Create The Task...
    $leitor = new Leitor;
    $leitor->nome = $request->nome;
    $leitor->email= $request->email;
    $leitor->save();

    return redirect('/');
});


/**
 * Add A New Task
 */
Route::post('/livros', function (Request $request) {
    //
     $validator = Validator::make($request->all(), [
        'titulo' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    // Create The Task...
    $livro = new Livro;
    $livro->titulo = $request->titulo;
    $livro->ano= $request->ano;
    $livro->save();

    return redirect('/');
});

/**
 * Delete An Existing Task
 */
Route::delete('/livros/{id}', function ($id) {
    //
    Livro::findOrFail($id)->delete();

    return redirect('/');
});

Route::get('/livros/{id}', function ($id) {
    return 'Livro '.$id;
});
