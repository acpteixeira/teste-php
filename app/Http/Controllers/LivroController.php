<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LivroController extends Controller
{ // agrupar toda a lógica das requisições em uma única classe
    public function index()
    {
        $livro = Livro::orderBy('created_at', 'desc')->paginate(10);
        return view('livros.index',['livros' => $livro]);
    }

    public function create()
    {
        return view('livros.create');
    }

    public function store(LivroRequest $book)
    {
        $livro = new Livro;
        $livro->titulo = $book->titulo;
        $livro->ano    = $book->ano;
        $livro->save();
        return redirect()->route('livros.index')->with('message', 'Livro criado com sucesso!');
    }

    public function show($id)
    {
        //
        return view('livros', ['livro' => Livro::findOrFail($id)]);
    }

    public function edit($id)
    {
        $livro = Livro::findOrFail($id);
        return view('livros.edit',compact('livro'));
    }

    public function update(LivroRequest $request, $id)
    {
        $livro = Livro::findOrFail($id);
        $livro->titulo = $request->titulo;
        $livro->ano = $request->ano;
        $livro->save();
        return redirect()->route('livros.index')->with('message', 'Livro atualizado com sucesso!');
    }

    public function destroy($id)
    {
        $livro = Livro::findOrFail($id);
        $livro->delete();
        return redirect()->route('livros.index')->with('alert-success','Livro deletado!');
    }
}
