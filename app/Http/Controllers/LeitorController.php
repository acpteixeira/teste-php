<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LeitorController extends Controller
{
    //

	public function index()
    {
        $leitor = Leitor::orderBy('created_at', 'desc')->paginate(10);
        return view('leitores.index',['leitores' => $leitores]);
    }

    public function create()
    {
        return view('leitores.create');
    }

    public function store(LeitorRequest $reader)
    {
        $leitor = new Leitor;
        $leitor->nome = $reader->titulo;
        $leitor->email    = $reader->email;
        $leitor->save();
        return redirect()->route('leitores.index')->with('message', 'Leitor criado!');
    }

    public function show($id)
    {
        //
        return view('leitores', ['leitor' => Leitor::findOrFail($id)]);
    }

    public function edit($id)
    {
        $leitor = Leitor::findOrFail($id);
        return view('leitores.edit',compact('leitor'));
    }

    public function update(LeitorRequest $request, $id)
    {
        $leitor = Product::findOrFail($id);
        $leitor->nome = $request->nome;
        $leitor->email = $request->email;
        $leitor->save();
        return redirect()->route('leitores.index')->with('message', 'Leitor atualizado!');
    }

    public function destroy($id)
    {
        $leitor = Leitor::findOrFail($id);
        $leitor->delete();
        return redirect()->route('leitores.index')->with('alert-success','Leitor deletado!');
    }
}


}
