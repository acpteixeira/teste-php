<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

// cada tabela de banco de dados tem um Model correspondente que é usado para interagir com essa tabela

class Leitor extends Model
{
    use Notifiable;

    protected $fillable = ['nome','email']; // campos que podem ser inseridos pelo usuário do sistema
    protected $table = 'leitores';

    // ver locacao associada ao leitor
    public function historico_leitor()
    {
        return $this->belongsTo('App\Locacao');
    }

}
